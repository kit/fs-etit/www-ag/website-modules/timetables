---
title: Timetables
menu:
    main:
        parent: studies
hideDate: true
---

On this page you will find the recommended timetables for your Bachelor's degree in Electrical and Computer Engineering.

{{< highlight >}}
Timetables for the study program Mechatronics and Information Technology can be found on the [website of the Working Group Mechatronics and Information Technology](https://ak-mit.vs.kit.edu/docs/organisatorisches/#stundenpl%C3%A4ne).
{{</ highlight >}}

{{< tab-header-bar >}}
    {{< tab-header-item "etit-tt" "Electrical Engineering and Information Technology (B.Sc.)" "true" >}}
    {{< tab-header-item "med-tt" "Medical Technology (B.Sc.)" "false" >}}
{{</ tab-header-bar >}}

{{< tab-content-container >}}
    {{< tab-content-item "etit-tt" "true" >}}
        {{< accordion "etit-timetables" >}}
            {{< accordion-item "Term 1" >}}
                {{< timetable "etit-semester1" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 2" >}}
                {{< timetable "etit-semester2" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 3" >}}
                {{< timetable "etit-semester3" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 4" >}}
                {{< timetable "etit-semester4" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 5" >}}
                {{< timetable "etit-semester5" >}}
            {{</ accordion-item >}}
        {{</ accordion >}}
    {{</ tab-content-item >}}
    {{< tab-content-item "med-tt" "false" >}}
        {{< accordion "med-timetables" >}}
            {{< accordion-item "Term 1" >}}
                {{< timetable "med-semester1" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 2" >}}
                {{< timetable "med-semester2" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 3" >}}
                {{< timetable "med-semester3" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Term 4" >}}
                {{< timetable "med-semester4" >}}
            {{</ accordion-item >}}
        {{</ accordion >}}
    {{</ tab-content-item >}}
{{</ tab-content-container >}}

Our student council meetings are held every Wednesday at 6:00 pm.

**Info:** Since WT 2015/16, the [Electrical Engineering and Information Technology Workshop](http://www.etit.kit.edu/1402.php) is mandatory in the 1st, 2nd and 3rd semester.
