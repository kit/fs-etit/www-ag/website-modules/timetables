---
title: Stundenpläne
menu:
    main:
        parent: studies
hideDate: true
---

Auf dieser Seite findest du die empfohlenen Stundenpläne für dein Bachelorstudium Elektro- und Informationstechnik.

{{< highlight >}}
Stundenpläne für den Studiengang Mechatronik und Informationstechnik findest du auf der [Webseite des Arbeitskreises Mechatronik und Informationstechnik](https://ak-mit.vs.kit.edu/docs/organisatorisches/#stundenpl%C3%A4ne).
{{</ highlight >}}

{{< tab-header-bar >}}
    {{< tab-header-item "etit-tt" "Elektro- und Informationstechnik (B.Sc.)" "true" >}}
    {{< tab-header-item "med-tt" "Medizintechnik (B.Sc.)" "false" >}}
{{</ tab-header-bar >}}

{{< tab-content-container >}}
    {{< tab-content-item "etit-tt" "true" >}}
        {{< accordion "etit-timetables" >}}
            {{< accordion-item "Semester 1" >}}
                {{< timetable "etit-semester1" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 2" >}}
                {{< timetable "etit-semester2" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 3" >}}
                {{< timetable "etit-semester3" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 4" >}}
                {{< timetable "etit-semester4" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 5" >}}
                {{< timetable "etit-semester5" >}}
            {{</ accordion-item >}}
        {{</ accordion >}}
    {{</ tab-content-item >}}
    {{< tab-content-item "med-tt" "false" >}}
        {{< accordion "med-timetables" >}}
            {{< accordion-item "Semester 1" >}}
                {{< timetable "med-semester1" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 2" >}}
                {{< timetable "med-semester2" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 3" >}}
                {{< timetable "med-semester3" >}}
            {{</ accordion-item >}}
            {{< accordion-item "Semester 4" >}}
                {{< timetable "med-semester4" >}}
            {{</ accordion-item >}}
        {{</ accordion >}}
    {{</ tab-content-item >}}
{{</ tab-content-container >}}

Unsere Fachschaftssitzungen finden jeden Mittwoch um 18:00 Uhr statt.

**Info:** Seit dem WS 2015/16 findet im 1., 2. und 3. Semester der [Workshop Elektrotechnik und Informationstechnik](http://www.etit.kit.edu/1402.php) verpflichtend statt.
